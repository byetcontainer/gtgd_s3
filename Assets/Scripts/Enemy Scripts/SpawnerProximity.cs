﻿using UnityEngine;
using System.Collections;

namespace s3 {
	public class SpawnerProximity : MonoBehaviour {

        //Variables go here
        public GameObject objectToSpawn;
        public int numberToSpawn;
        public float proximity;
        private float checkRate;
        private float nextcheck;
        private Transform playerTransform;
        private Vector3 spawnPosition;

	
		void Start () {
            SetReferences();
        }
		
		void Update () {
            CheckDistance();
		}
		
		void SetReferences (){
            playerTransform = GameManager_References._playerObject.transform;
            checkRate = Random.Range(0.8f, 1.2f);
		}

        void CheckDistance() {
            if (Time.time > nextcheck) {
                nextcheck = Time.time + checkRate;
                if (Vector3.Distance(transform.position, playerTransform.position) < proximity) {
                    SpawnObjects();
                    this.enabled = false;
                }
            }
        }

        void SpawnObjects() {
            for (int i = 0; i < numberToSpawn; i++) {
                spawnPosition = transform.position + Random.insideUnitSphere * 5;
                Instantiate(objectToSpawn, spawnPosition, transform.rotation);
            }
        }

	}
}
