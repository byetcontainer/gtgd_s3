﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Enemy_Attack : MonoBehaviour
    {
        private Enemy_MasterScript enemyMaster;
        private Transform attackTarget;

        public float attackRate = 1;
        public float nextAttack;
        public float attackRange = 3.5f;
        public int attackDamage = 10;


        void OnEnable() {
            SetReferences();
            enemyMaster.EventEnemyDie += DisableThis;
            enemyMaster.EventEnemySetNavTarget += SetAttackTarget;
        }

        void OnDisable() {
            enemyMaster.EventEnemyDie -= DisableThis;
            enemyMaster.EventEnemySetNavTarget -= SetAttackTarget;
        }

        void SetReferences() {
            enemyMaster = GetComponent<Enemy_MasterScript>();
        }

        void Update()
        {
            TryToAttack();
        }

        void SetAttackTarget(Transform targetTransform) {
            attackTarget = targetTransform;
        }

        void TryToAttack() {
            if(attackTarget != null) {
                if (Time.time > nextAttack) {
                    nextAttack = Time.time + attackRate;
                    if (Vector3.Distance(transform.position, attackTarget.position) <= attackRange) {
                        Vector3 lookAtVector = new Vector3(attackTarget.position.x, transform.position.y, attackTarget.position.z);
                        transform.LookAt(lookAtVector);
                        enemyMaster.CallEventEnemyAttack();
                        enemyMaster.isOnRoute = false;
                    }
                }    
            }
        }

        public void OnEnemyAttack() { //called by hPunch animation
            if (attackTarget != null) {
                if (Vector3.Distance(transform.position, attackTarget.position) <= attackRange &&
                    attackTarget.GetComponent<Player_MasterScript>() != null)
                {
                    Vector3 toOther = attackTarget.position - transform.position;
                    if (Vector3.Dot(toOther, transform.forward) > 0.5f) {
                        attackTarget.GetComponent<Player_MasterScript>().CallEventPlayerHealthDeduction(attackDamage);
                    }
                }
            }
        }

        void DisableThis() {
            this.enabled = false;
        }


    }

}
