﻿using UnityEngine;
using System.Collections;

namespace s3 {
	public class Enemy_CollisionField : MonoBehaviour {

        //Variables go here
        private Enemy_MasterScript enemyMaster;
        private Rigidbody rigidBodyStrikingMe;
        private int damageToApply;
        public float massRequirement = 50;
        public float speedRequirement = 5;
        private float damageFactor = 0.1f;
	
		void OnEnable (){
            SetReferences();
            enemyMaster.EventEnemyDie += DisableThis;
		}
		
		void OnDisable (){
            enemyMaster.EventEnemyDie -= DisableThis;
        }

        void OnTriggerEnter(Collider other) {
            if (other.GetComponent<Rigidbody>() != null) {
                rigidBodyStrikingMe = other.GetComponent<Rigidbody>();

                if (rigidBodyStrikingMe.mass >= massRequirement && rigidBodyStrikingMe.velocity.sqrMagnitude > speedRequirement * speedRequirement) {
                    damageToApply = (int) (damageFactor * rigidBodyStrikingMe.mass * rigidBodyStrikingMe.velocity.magnitude);
                    enemyMaster.CallEventEnemyDeductHealth(damageToApply);
                }
            }
        }
		
		void SetReferences (){
            enemyMaster = transform.root.GetComponent<Enemy_MasterScript>();
		}

        void DisableThis() {
            gameObject.SetActive(false);
        }
		
	}
}
