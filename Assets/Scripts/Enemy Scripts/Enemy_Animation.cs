﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Enemy_Animation : MonoBehaviour
    {

        private Enemy_MasterScript enemyMaster;
        private Animator anim;

        void OnEnable() {
            SetReferences();
            enemyMaster.EventEnemyDie += DisableAnimations;
            enemyMaster.EventEnemyWalking += SetAnimationToWalk;
            enemyMaster.EventEnemyReachedNavTarget += SetAnimationToIdle;
            enemyMaster.EventEnemyAttack += SetAnimationToAttack;
            enemyMaster.EventEnemyDeductHealth += SetAnimationToStruck;
        }

        void OnDisable() {
            enemyMaster.EventEnemyDie -= DisableAnimations;
            enemyMaster.EventEnemyWalking -= SetAnimationToWalk;
            enemyMaster.EventEnemyReachedNavTarget -= SetAnimationToIdle;
            enemyMaster.EventEnemyAttack -= SetAnimationToAttack;
            enemyMaster.EventEnemyDeductHealth -= SetAnimationToStruck;
        }

        void SetReferences() {
            enemyMaster = GetComponent<Enemy_MasterScript>();

            if (GetComponent<Animator>() != null) {
                anim = GetComponent<Animator>();
            }
        }

        void SetAnimationToWalk() {
            if (anim != null) {
                if (anim.enabled)
                {
                    anim.SetBool("isPursuing", true);
                }
            }
        }

        void SetAnimationToIdle()
        {
            if (anim != null)
            {
                if (anim.enabled)
                {
                    anim.SetBool("isPursuing", false);
                }
            }
        }

        void SetAnimationToAttack()
        {
            if (anim != null)
            {
                if (anim.enabled)
                {
                    anim.SetTrigger("Attack");
                }
            }
        }

        void SetAnimationToStruck(int dummy)
        {
            if (anim != null)
            {
                if (anim.enabled)
                {
                    anim.SetTrigger("Struck");
                }
            }
        }

        void DisableAnimations() {
            if(anim != null)
            {
                anim.enabled = false;
            }
        }
    }

}
