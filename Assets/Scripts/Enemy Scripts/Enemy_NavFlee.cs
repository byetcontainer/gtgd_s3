﻿using UnityEngine;
using System.Collections;

namespace s3 {
	public class Enemy_NavFlee : MonoBehaviour {

        public bool isFleeing;
        private Enemy_MasterScript enemyMaster;
        private NavMeshAgent myNavMeshAgent;
        private NavMeshHit navHit;
        public Transform fleeTarget;
        private Vector3 runPosition;
        private Vector3 directionToPlayer;
        public float fleeRange = 25;
        private float checkRate;
        private float nextCheck;
	
		void OnEnable (){
			SetReferences();
            enemyMaster.EventEnemyDie += DisableThis;
            enemyMaster.EventEnemySetNavTarget += SetFleeTarget;
            enemyMaster.EventEnemyHealthLow += IShouldFlee;
            enemyMaster.EventEnemyHealthRecovered += IShouldStopFleeing;
		}
		
		void OnDisable (){
            enemyMaster.EventEnemyDie -= DisableThis;
            enemyMaster.EventEnemySetNavTarget -= SetFleeTarget;
            enemyMaster.EventEnemyHealthLow -= IShouldFlee;
            enemyMaster.EventEnemyHealthRecovered -= IShouldStopFleeing;
        }
	
		
		void Update () {
            if (Time.time > nextCheck) {
                nextCheck = Time.time + checkRate;

                CheckIfIShouldFlee();
            }
		}
		
		void SetReferences (){
            enemyMaster = GetComponent<Enemy_MasterScript>();
            if (GetComponent<NavMeshAgent>() != null) {
                myNavMeshAgent = GetComponent<NavMeshAgent>();
            }
            checkRate = Random.Range(0.3f, 0.4f);
		}

        void SetFleeTarget(Transform target) {
            fleeTarget = target;
        }

        void IShouldFlee() {
            isFleeing = true;

            if (GetComponent<Enemy_NavPursue>() != null) {
                GetComponent<Enemy_NavPursue>().enabled = false;
            }
        }

        void IShouldStopFleeing() {
            isFleeing = false;

            if (GetComponent<Enemy_NavPursue>() != null) {
                GetComponent<Enemy_NavPursue>().enabled = true;
            }
        }

        void CheckIfIShouldFlee() {
            if (isFleeing) {
                if (fleeTarget != null && !enemyMaster.isOnRoute && !enemyMaster.isNavPaused) {
                    if(FleeTarget(out runPosition) && Vector3.Distance(transform.position, fleeTarget.position) < fleeRange){
                        myNavMeshAgent.SetDestination(runPosition);
                        enemyMaster.CallEventEnemyWalking();
                        enemyMaster.isOnRoute = true;
                    }
                }
            }
        }

        bool FleeTarget(out Vector3 result) {
            directionToPlayer = transform.position - fleeTarget.position;
            Vector3 checkPos = transform.position + directionToPlayer;

            if (NavMesh.SamplePosition(checkPos, out navHit, 1.0f, NavMesh.AllAreas))
            {
                result = navHit.position;
                return true;
            }
            else {
                result = transform.position;
                return false;
            }
        }

        void DisableThis() {
            this.enabled = false;
        }

	}
}
