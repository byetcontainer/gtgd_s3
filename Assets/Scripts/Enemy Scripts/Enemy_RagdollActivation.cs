﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Enemy_RagdollActivation : MonoBehaviour
    {
        private Enemy_MasterScript enemyMaster;
        private Collider myCollider;
        private Rigidbody myRigidbody;

        void OnEnable() {
            SetReferences();
            enemyMaster.EventEnemyDie += ActivateRagdoll;
        }

        void OnDisable() {
            enemyMaster.EventEnemyDie -= ActivateRagdoll;
        }

        void SetReferences() {
            enemyMaster = transform.root.GetComponent<Enemy_MasterScript>();

            if (GetComponent<Collider>() != null) {
                myCollider = GetComponent<Collider>();
            }

            if (GetComponent<Rigidbody>() != null) {
                myRigidbody = GetComponent<Rigidbody>();
            }
        }

        void ActivateRagdoll() {
            if (myRigidbody != null) {
                myRigidbody.isKinematic = false;
                myRigidbody.useGravity = true;
            }

            if (myCollider != null) {
                myCollider.isTrigger = false;
                myCollider.enabled = true;
            }
        }


    }

}
