﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Enemy_NavDestinationReached : MonoBehaviour
    {
        private Enemy_MasterScript enemyMaster;
        private NavMeshAgent myNavMeshAgent;
        private float checkRate;
        private float nextCheck;

        void OnEnable()
        {
            SetReferences();
            enemyMaster.EventEnemyDie += DisableThis;
        }

        void OnDisable()
        {
            enemyMaster.EventEnemyDie -= DisableThis;
        }


        void Update(){
            if(Time.time > nextCheck)
            {
                nextCheck = Time.time + checkRate;
                CheckIfDestinationReached();
            }
        }

        void SetReferences(){
            enemyMaster = GetComponent<Enemy_MasterScript>();

            if (GetComponent<NavMeshAgent>() != null){
                myNavMeshAgent = GetComponent<NavMeshAgent>();
            }

            checkRate = Random.Range(0.3f, 0.4f);
        }

        void CheckIfDestinationReached() {
            if (enemyMaster.isOnRoute) {
                if(myNavMeshAgent.remainingDistance < myNavMeshAgent.stoppingDistance)
                {
                    enemyMaster.isOnRoute = false;
                    enemyMaster.CallEventReachedNavTarget(); //<--CallEvent
                }
            }
        }

        void DisableThis()
        {
            this.enabled = false;
        }
    }

}
