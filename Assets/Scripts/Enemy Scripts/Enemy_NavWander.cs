﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Enemy_NavWander: MonoBehaviour
    {
        private Enemy_MasterScript enemyMaster;
        private NavMeshAgent myNavMeshAgent;
        private float checkRate;
        private float nextCheck;
        private float wanderRange = 10f;
        private NavMeshHit navHit;
        private Vector3 wanderTarget;

        void OnEnable()
        {
            SetReferences();
            enemyMaster.EventEnemyDie += DisableThis;
        }

        void OnDisable()
        {
            enemyMaster.EventEnemyDie -= DisableThis;
        }


        void Update(){
            if(Time.time > nextCheck)
            {
                nextCheck = Time.time + checkRate;
                CheckIfIShouldWander();
            }
        }

        void SetReferences(){
            enemyMaster = GetComponent<Enemy_MasterScript>();

            if (GetComponent<NavMeshAgent>() != null){
                myNavMeshAgent = GetComponent<NavMeshAgent>();
            }

            checkRate = Random.Range(0.3f, 0.4f);
        }

        void CheckIfIShouldWander() {
            if(enemyMaster.myTarget == null && !enemyMaster.isOnRoute && !enemyMaster.isNavPaused)
            {
                if (RandomWanderTarget(transform.position, wanderRange, out wanderTarget))
                {
                    myNavMeshAgent.SetDestination(wanderTarget);
                    enemyMaster.isOnRoute = true;
                    enemyMaster.CallEventEnemyWalking();
                }
            }
        }

        bool RandomWanderTarget(Vector3 center, float range, out Vector3 result)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * wanderRange;
            if (NavMesh.SamplePosition(randomPoint, out navHit, 1.0f, NavMesh.AllAreas))
            {
                result = navHit.position;
                return true;
            }
            else
            {
                result = center;
                return false;
            }
        }

        void DisableThis()
        {
            this.enabled = false;
        }
    }

}
