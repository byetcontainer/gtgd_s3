﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Enemy_TakeDamage : MonoBehaviour
    {
        private Enemy_MasterScript enemyMaster;
        public int damageMultiplier = 1;
        public bool shouldRemoveCollider;

        void OnEnable()
        {
            SetReferences();
            enemyMaster.EventEnemyDie += RemoveThis;
        }

        void OnDisable()
        {
            enemyMaster.EventEnemyDie -= RemoveThis;
        }

        void SetReferences()
        {
            enemyMaster = transform.root.GetComponent<Enemy_MasterScript>();
        }

        public void ProcessDamage(int damage)
        {
            int damageToApply = damage * damageMultiplier;
            enemyMaster.CallEventEnemyDeductHealth(damageToApply);
        }

        void RemoveThis()
        {
            if (shouldRemoveCollider)
            {
                if (GetComponent<Collider>() != null)
                {
                    Destroy(GetComponent<Collider>());
                }
            }


            if (GetComponent<Rigidbody>() != null && GetComponent<CharacterJoint>() != null)
            {
                Destroy(GetComponent<CharacterJoint>());
                Destroy(GetComponent<Rigidbody>());
            }

            Destroy(this);

        }

    }

}
