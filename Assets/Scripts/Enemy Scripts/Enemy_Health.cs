﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Enemy_Health : MonoBehaviour
    {
        private Enemy_MasterScript enemyMaster;
        public int enemyHealth = 100;
        private float healthLow = 25;

        void OnEnable() {
            SetReferences();
            enemyMaster.EventEnemyDeductHealth += DeductHealth;
            enemyMaster.EventEnemyIncreaseHealth += IncreaseHealth;
        }

        void OnDisable() {
            enemyMaster.EventEnemyDeductHealth -= DeductHealth;
            enemyMaster.EventEnemyIncreaseHealth -= IncreaseHealth;
        }

        void Update() {
            if (Input.GetKeyUp(KeyCode.B)) {
                enemyMaster.CallEventEnemyIncreaseHealth(70);
            }
        }

        void SetReferences() {
            enemyMaster = GetComponent<Enemy_MasterScript>();
        }

        void DeductHealth(int healthChange) {
            enemyHealth -= healthChange;
            if (enemyHealth <= 0) {
                enemyHealth = 0;
                enemyMaster.CallEventEnemyDie();
                Destroy(gameObject, Random.Range(10, 20));
            }

            CheckHealthFraction();
        }

        void CheckHealthFraction() {
            if (enemyHealth <= healthLow && enemyHealth > 0) {
                enemyMaster.CallEventEnemyHealthLow();
            }
            else if (enemyHealth > healthLow) {
                enemyMaster.CallEventEnemyHealthRecovered();
            }
        }

        void IncreaseHealth(int healthChange) {
            enemyHealth += healthChange;
            if (enemyHealth > 100) {        //Clamp value to 100
                enemyHealth = 100;
            }

            CheckHealthFraction();
        }


    }

}
