﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Enemy_NavPause : MonoBehaviour
    {
        private Enemy_MasterScript enemyMaster;
        private NavMeshAgent myNavMeshAgent;
        private float pauseTime = 1;

        void OnEnable() {
            SetReferences();
            enemyMaster.EventEnemyDie += DisableThis;
            enemyMaster.EventEnemyDeductHealth += PauseNavMeshAgent;
        }

        void OnDisable() {
            enemyMaster.EventEnemyDie -= DisableThis;
            enemyMaster.EventEnemyDeductHealth -= PauseNavMeshAgent;
        }

        void Update()
        {

        }

        void SetReferences()
        {
            enemyMaster = GetComponent<Enemy_MasterScript>();
            myNavMeshAgent = GetComponent<NavMeshAgent>();
        }

        void PauseNavMeshAgent(int dummy)
        {
            if (myNavMeshAgent != null) {
                myNavMeshAgent.ResetPath();
                enemyMaster.isNavPaused = true;
                StartCoroutine(RestartNavMeshAgent());
            }
        }

        IEnumerator RestartNavMeshAgent()
        {
            yield return new WaitForSeconds(pauseTime);
            enemyMaster.isNavPaused = false;
        }

        void DisableThis() {
            StopAllCoroutines();
        }

    }

}
