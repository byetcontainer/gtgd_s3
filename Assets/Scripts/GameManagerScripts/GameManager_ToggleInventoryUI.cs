﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class GameManager_ToggleInventoryUI : MonoBehaviour
    {
        [Tooltip("Does this gamemode have an inventory?")]
        public bool hasInventory;
        public GameObject inventoryUI;
        public string toggleInventoryKey;

        private GameManager_MasterScript gameManagerMaster;

        void Start() {
            SetReferences();
        }

        void Update()
        {
            CheckForInventoryUIToggleRequest();
        }

        void SetReferences() {
            gameManagerMaster = GetComponent<GameManager_MasterScript>();

            if (toggleInventoryKey == "") {
                Debug.LogWarning("Missing name for toggle inventory button");
                this.enabled = false;
            }
        }

        void CheckForInventoryUIToggleRequest() {
            if (Input.GetButtonUp(toggleInventoryKey) && !gameManagerMaster.isMenuOn && !gameManagerMaster.isGameOver && hasInventory) {
                ToggleInventory();
            }
        }

        public void ToggleInventory() {
            if (inventoryUI != null) {
                inventoryUI.SetActive(!inventoryUI.activeSelf);
                gameManagerMaster.isInventoryUIOn = !gameManagerMaster.isInventoryUIOn;
                gameManagerMaster.CallEventInventoryUIToggle();
            }
        }
    }

}
