﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class GameManager_TogglePause : MonoBehaviour
    {
        private GameManager_MasterScript gameManagerMaster;
        public bool isPaused;

        void OnEnable() {
            SetReferences();
            gameManagerMaster.MenuToggleEvent += TogglePause;
            gameManagerMaster.InventoryUIToggleEvent += TogglePause;
        }

        void OnDisable() {
            gameManagerMaster.MenuToggleEvent -= TogglePause;
            gameManagerMaster.InventoryUIToggleEvent -= TogglePause;
        }

        void SetReferences() {
            gameManagerMaster = GetComponent<GameManager_MasterScript>();
        }

        void TogglePause() {
            if (isPaused == true) {
                Time.timeScale = 1;
                isPaused = false;
            }
            else { 
                Time.timeScale = 0;
                isPaused = true;
            }
        }
    }

}
