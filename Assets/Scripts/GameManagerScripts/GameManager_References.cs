﻿using UnityEngine;
using System.Collections;


namespace s3 {
    public class GameManager_References : MonoBehaviour
    {
        public string playerTag;
        public static string _playerTag;

        public string enemyTag;
        public static string _enemyTag;

        public static GameObject _playerObject;

        void OnEnable() {
            if (playerTag == "") {
                Debug.LogWarning("Player tag name not set in GameManagerReferences");
            }

            if (enemyTag == "") {
                Debug.LogWarning("Enemy tag name not set in the GameManagerReferences");
            }

            _playerTag = playerTag;
            _enemyTag = enemyTag;

            _playerObject = GameObject.FindGameObjectWithTag(_playerTag);
        }
        
    }

}
