﻿using UnityEngine;
using System.Collections;


namespace s3 {
    public class GameManager_ToggleMenu : MonoBehaviour
    {
        private GameManager_MasterScript gameManagerMaster;
        public GameObject menu;

        void Start() {
            //ToggleMenu();
            gameManagerMaster.CallEventMenuToggle();
        }

        void Update() {
            CheckForMenuToggleRequest();
        }

        void OnEnable() {
            SetReferences();
            gameManagerMaster.GameOverEvent += ToggleMenu;
            gameManagerMaster.MenuToggleEvent += ToggleMenu;
        }

        void OnDisable() {
            gameManagerMaster.GameOverEvent -= ToggleMenu;
            gameManagerMaster.MenuToggleEvent -= ToggleMenu;
        }

        void SetReferences() {
            gameManagerMaster = GetComponent<GameManager_MasterScript>();
        }

        void CheckForMenuToggleRequest() {
            if (Input.GetKeyUp(KeyCode.Escape) && !gameManagerMaster.isGameOver && !gameManagerMaster.isInventoryUIOn) {
                //ToggleMenu();
                gameManagerMaster.CallEventMenuToggle();
            }
        }

        void ToggleMenu() {
            if (menu != null)
            {
                menu.SetActive(!menu.activeSelf);
                gameManagerMaster.isMenuOn = !gameManagerMaster.isMenuOn;
            }
            else {
                Debug.LogWarning("UI GameObject for Menu not Assigned to Toggle Menu Script in Inspector");
            }
        }
    }

}
