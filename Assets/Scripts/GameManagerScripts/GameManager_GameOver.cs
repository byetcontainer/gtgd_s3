﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class GameManager_GameOver : MonoBehaviour
    {
        private GameManager_MasterScript gameManagerMaster;
        public GameObject panelGameOver;
        // Use this for initialization
        void OnEnable() {
            SetReferences();
            gameManagerMaster.GameOverEvent += TurnOnGameOverPanel;
        }

        void OnDisable() {
            gameManagerMaster.GameOverEvent -= TurnOnGameOverPanel;
        }

        void SetReferences()
        {
            gameManagerMaster = GetComponent<GameManager_MasterScript>();
        }

        void TurnOnGameOverPanel() {
            if (panelGameOver != null) {
                panelGameOver.SetActive(true);
            }
        }
    }

}
