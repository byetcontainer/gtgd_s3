﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class GameManager_ToggleCursor : MonoBehaviour
    {
        private GameManager_MasterScript gameManagerMaster;
        private bool isCursorLocked = true;

        void OnEnable() {
            SetReferences();
            gameManagerMaster.MenuToggleEvent += ToggleCursorState;
            gameManagerMaster.InventoryUIToggleEvent += ToggleCursorState;
        }

        void OnDisable() {
            gameManagerMaster.MenuToggleEvent -= ToggleCursorState;
            gameManagerMaster.InventoryUIToggleEvent -= ToggleCursorState;
        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            CheckIfCursorShouldBeLocked();
        }


        void SetReferences() {
            gameManagerMaster = GetComponent<GameManager_MasterScript>();
        }

        void ToggleCursorState() {
            isCursorLocked = !isCursorLocked;
        }

        void CheckIfCursorShouldBeLocked() {
            if (isCursorLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }

        }
    }

}
