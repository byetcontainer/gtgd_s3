﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace s3 {
    public class GameManager_RestartLevel : MonoBehaviour
    {
        private GameManager_MasterScript gameManagerScript;

        void OnEnable() {
            SetReferences();
            gameManagerScript.RestartLevelEvent += RestartLevel;
        }

        void OnDisable()
        {
            gameManagerScript.RestartLevelEvent -= RestartLevel;
        }

        void SetReferences() {
            gameManagerScript = GetComponent<GameManager_MasterScript>();
        }

        void RestartLevel() {
            //Application.LoadLevel(Application.loadedLevel);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

}
