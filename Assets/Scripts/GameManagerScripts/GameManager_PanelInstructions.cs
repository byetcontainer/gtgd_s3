﻿using UnityEngine;
using System.Collections;
namespace s3 {
    public class GameManager_PanelInstructions : MonoBehaviour
    {
        public GameObject panelInstructions;
        private GameManager_MasterScript gameManagerMaster;

        void OnEnable() {
            SetReferences();
            gameManagerMaster.GameOverEvent += TurnOffPanelInstructions;
        }

        void OnDisable() {
            gameManagerMaster.GameOverEvent -= TurnOffPanelInstructions;
        }

        void SetReferences() {
            gameManagerMaster = GetComponent<GameManager_MasterScript>();
        }

        void TurnOffPanelInstructions() {
            if (panelInstructions != null) {
                panelInstructions.SetActive(false);            }
        }
    }

}
