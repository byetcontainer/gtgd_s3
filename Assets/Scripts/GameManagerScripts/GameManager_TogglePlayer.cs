﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

namespace s3 {
    public class GameManager_TogglePlayer : MonoBehaviour
    {
        public FirstPersonController playerController;
        private GameManager_MasterScript gameManagerMaster;

        void OnEnable() {
            SetReferences();
            gameManagerMaster.MenuToggleEvent += ToggleController;
            gameManagerMaster.InventoryUIToggleEvent += ToggleController;
        }

        void OnDisable() {
            gameManagerMaster.MenuToggleEvent -= ToggleController;
            gameManagerMaster.InventoryUIToggleEvent -= ToggleController;
        }


        void SetReferences() {
            gameManagerMaster = GetComponent<GameManager_MasterScript>();
        }

        void ToggleController() {
            if (playerController != null) {
                playerController.enabled = !playerController.enabled;
            }
        }


    }

}

