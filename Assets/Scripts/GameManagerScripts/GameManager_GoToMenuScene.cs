﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace s3 {
    public class GameManager_GoToMenuScene : MonoBehaviour
    {
        private GameManager_MasterScript gameManagerMaster;


        void OnEnable() {
            SetReferences();
            gameManagerMaster.GoToMenuSceneEvent += GoToMenuScene;
        }

        void OnDisable() {
            gameManagerMaster.GoToMenuSceneEvent -= GoToMenuScene;
        }

        void SetReferences() {
            gameManagerMaster = GetComponent<GameManager_MasterScript>();
        }

        void GoToMenuScene() {
            SceneManager.LoadScene(0);
        }
    }

}
