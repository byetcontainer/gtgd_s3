﻿using UnityEngine;
using System.Collections;


namespace s3 {
    public class Player_CanvasHurt : MonoBehaviour
    {
        public GameObject hurtCanvas;
        private Player_MasterScript playerMaster;
        private float secondsTilHide = 2;

        void OnEnable() {
            SetReferences();
            playerMaster.EventPlayerHealthDeduction += TurnOnHurtEffect;
        }

        void OnDisable() {
            playerMaster.EventPlayerHealthDeduction -= TurnOnHurtEffect;
        }

        void TurnOnHurtEffect(int dummy) {
            if (hurtCanvas != null) {
                StopAllCoroutines();
                hurtCanvas.SetActive(true);
                StartCoroutine(ResetHurtCanvas());
            }
        }

        void SetReferences() {
            playerMaster = GetComponent<Player_MasterScript>();
        }

        IEnumerator ResetHurtCanvas() {
            yield return new WaitForSeconds(secondsTilHide);
            hurtCanvas.SetActive(false);
        }

    }
}


