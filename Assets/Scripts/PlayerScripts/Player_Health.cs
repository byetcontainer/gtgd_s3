﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace s3 {
    public class Player_Health : MonoBehaviour
    {
        private GameManager_MasterScript gameManagerMaster;
        private Player_MasterScript playerMaster;
        public int playerHealth;
        public Text healthText;

        void OnEnable() {
            SetReferences();
            SetUI();
            playerMaster.EventPlayerHealthDeduction += DeductHealth;
            playerMaster.EventPlayerHealthIncrease += IncreaseHealth;
        }

        void OnDisable() {
            playerMaster.EventPlayerHealthDeduction -= DeductHealth;
            playerMaster.EventPlayerHealthIncrease -= IncreaseHealth;
        }

        // Use this for initialization
        void Start()
        {
            //StartCoroutine(TestHealthDeduction());   
        }

        void SetReferences() {
            gameManagerMaster = GameObject.Find("GameManager").GetComponent<GameManager_MasterScript>();
            playerMaster = GetComponent<Player_MasterScript>();
        }

        IEnumerator TestHealthDeduction() {
            yield return new WaitForSeconds(4);
            //DeductHealth(100);
            playerMaster.CallEventPlayerHealthDeduction(50);
        }

        void DeductHealth(int healthChange) {
            playerHealth -= healthChange;
            if (playerHealth <= 0) {
                playerHealth = 0;
                gameManagerMaster.CallEventGameOver();
            }

            SetUI();
        }

        void IncreaseHealth(int healthChange) {
            playerHealth += healthChange;
            if (playerHealth > 100) {
                playerHealth = 100;
            }

            SetUI();
        }

        void SetUI() {
            if (healthText != null) {
                healthText.text = playerHealth.ToString();
            }
        }
    }

}
