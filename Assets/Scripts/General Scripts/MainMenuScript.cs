﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace s3 {
    public class MainMenuScript : MonoBehaviour
    {
        public void PlayGame() {
            SceneManager.LoadScene(1);
        }

        public void ExitGame() {
            Application.Quit();
        }




    }

}
