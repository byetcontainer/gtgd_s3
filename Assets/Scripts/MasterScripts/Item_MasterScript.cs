﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_MasterScript : MonoBehaviour
    {
        private Player_MasterScript playerMasterScript;

        public delegate void GeneralEventHandler();
        public event GeneralEventHandler EventObjectThrow;
        public event GeneralEventHandler EventObjectPickup;

        public delegate void PickupActionEventHandler(Transform item);
        public event PickupActionEventHandler EventPickupAction;

        void OnEnable() {
            SetReferences();
        }

        void OnDisable() {

        }

        void Start() {
            //SetReferences();
        }

        void SetReferences()
        {
            if (GameManager_References._playerObject != null)
            {
                playerMasterScript = GameManager_References._playerObject.GetComponent<Player_MasterScript>();
            }
        }

        public void CallEventObjectThrow() {
            if (EventObjectThrow != null) {
                EventObjectThrow();
                
            }

            playerMasterScript.CallEventHandsEmpty();
            playerMasterScript.CallEventInventoryChanged();

            if (EventObjectThrow == null) {
                Debug.Log("EventObjectThrow == null");
            }
        }

        public void CallEventObjectPickup() {
            if (EventObjectPickup != null) {
                EventObjectPickup();
                
            }

            playerMasterScript.CallEventHandsEmpty();

            if (EventObjectPickup == null)
            {
                //Debug.Log("EventObjectPickup == null");
            }
        }

        public void CallEventPickupAction(Transform item) {
            if (EventPickupAction != null) {
                EventPickupAction(item);
            }

            playerMasterScript.CallEventInventoryChanged();

            if (EventPickupAction == null)
            {
                //Debug.Log("EventObjectAction == null");
            }
        }


    }

}
