﻿using UnityEngine;
using System.Collections;

namespace s3 {
	public class Gun_ApplyDamageScript : MonoBehaviour {

        //Variables go here
        private Gun_Master gunmaster;
        public int damage = 10;
        		
	
		void OnEnable (){
			SetReferences();
            gunmaster.EventShotEnemy += ApplyDamage;
		}
		
		void OnDisable (){
            gunmaster.EventShotEnemy -= ApplyDamage;
        }
	
		
		void SetReferences (){
            gunmaster = GetComponent<Gun_Master>();
		}

        void ApplyDamage(Vector3 hitPosition, Transform hitTransform) {
            if (hitTransform.GetComponent<Enemy_TakeDamage>() != null) {
                hitTransform.GetComponent<Enemy_TakeDamage>().ProcessDamage(damage);
            }
        }
		
	}
}
