﻿using UnityEngine;
using System.Collections;

namespace s3 {
	public class Gun_MuzzleFlash : MonoBehaviour {

        //Variables go here
        public ParticleSystem muzzleFlash;
        private Gun_Master gunMaster;		
	
		void OnEnable (){
			SetReferences();
            gunMaster.EventPlayerInput += PlayMuzzleFlash;
		}
		
		void OnDisable (){
            gunMaster.EventPlayerInput -= PlayMuzzleFlash;
        }
	
		void Start () {
			
		}
		
		void Update () {
			
		}
		
		void SetReferences (){
            gunMaster = GetComponent<Gun_Master>();
		}

        void PlayMuzzleFlash() {
            if (muzzleFlash != null) {
                muzzleFlash.Play();
            }
        }
		
	}
}
