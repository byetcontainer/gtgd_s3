﻿using UnityEngine;
using System.Collections;

namespace s3 {
	public class Gun_Sounds : MonoBehaviour {

        //Variables go here
        private Gun_Master gunMaster;
        public float shootVolume = 0.4f;
        public float reloadVolume = 0.5f;
        public AudioClip[] shootSound;
        public AudioClip reloadSound;
	
		void OnEnable (){
			SetReferences();
            gunMaster.EventPlayerInput += PlayShootSound;
		}
		
		void OnDisable (){
            gunMaster.EventPlayerInput -= PlayShootSound;
        }

		
		void SetReferences (){
            gunMaster = GetComponent<Gun_Master>();
		}

        void PlayShootSound() {
            if (shootSound.Length > 0) {
                int index = Random.Range(0, shootSound.Length);
                AudioSource.PlayClipAtPoint(shootSound[index], transform.position, shootVolume);
            }
        }


        public void PlayReloadSound() {
            if (reloadSound != null) {
                AudioSource.PlayClipAtPoint(reloadSound, transform.position, reloadVolume);
            }
        }
		
	}
}
