﻿using UnityEngine;
using System.Collections;

namespace s3 {
	public class Gun_Animator : MonoBehaviour {

        //Variables go here
        private Gun_Master gunMaster;
        private Animator myAnimator;
	
		void OnEnable (){
			SetReferences();
            gunMaster.EventPlayerInput += PlayShootAnimation;
		}
		
		void OnDisable (){
            gunMaster.EventPlayerInput -= PlayShootAnimation;
        }
		
		void SetReferences (){
            gunMaster = GetComponent<Gun_Master>();

            if (GetComponent<Animator>() != null) {
                myAnimator = GetComponent<Animator>();
            }
		}

        void PlayShootAnimation() {
            if (myAnimator != null) {
                myAnimator.SetTrigger("Shoot");
            }
        }

	}
}
