﻿using UnityEngine;
using System.Collections;

namespace s3 {
	public class Gun_HitEffects : MonoBehaviour {

        //Variables go here
        private Gun_Master gunMaster;
        public GameObject defaultHitEffect;
        public GameObject enemyHitEffect;

        void OnEnable (){
			SetReferences();
            gunMaster.EventShotDefault += SpawnDefaultHitEffect;
            gunMaster.EventShotEnemy += SpawnEnemyHitEffect;
		}
		
		void OnDisable (){
            gunMaster.EventShotDefault -= SpawnDefaultHitEffect;
            gunMaster.EventShotEnemy -= SpawnEnemyHitEffect;
        }
		
		void SetReferences (){
            gunMaster = GetComponent<Gun_Master>();
		}

        void SpawnDefaultHitEffect(Vector3 hitPosition, Transform hitTransform) {
            if (defaultHitEffect != null)
            {
                Instantiate(defaultHitEffect, hitPosition, Quaternion.identity);
            }
        }

        void SpawnEnemyHitEffect(Vector3 hitPosition, Transform hitTransform) {
            if (enemyHitEffect != null) {
                Instantiate(enemyHitEffect, hitPosition, Quaternion.identity);
            }
        }
		
	}
}
