﻿using UnityEngine;
using System.Collections;


namespace Chapter1 {
    public class TriggerExampleScript : MonoBehaviour
    {
        private GameManager_EventMasterScript eventMasterScript;

        void Start() {
            SetReferences();
        }

        void SetReferences() {
            eventMasterScript = GameObject.Find("GameManager").GetComponent<GameManager_EventMasterScript>();
            
        }

        void OnTriggerEnter(Collider other) {
            //Debug.Log(other.name + "has entered");
            eventMasterScript.CallMyGeneralEvent();
            Destroy(gameObject);
        }

        //void OnTriggerStay(Collider other) {
        //    Debug.Log(other.name + "is in the triggered");
        //}
    }

}
