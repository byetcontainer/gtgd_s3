﻿using UnityEngine;
using System.Collections;

namespace Chapter1
{
    public class ShootScript : MonoBehaviour
    {

        private float fireRate = 0.3f;
        private float nextFire;
        private RaycastHit hit;
        private float range = 300f;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            CheckForInput();
        }

        void SetReferences() {

        }

        void CheckForInput()
        {
            if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
            {

                Debug.DrawRay(transform.TransformPoint(0,0,1), transform.forward, Color.green, 3);
                if (Physics.Raycast(transform.TransformPoint(0,0,1), transform.forward, out hit, range))
                {
                    if (hit.transform.CompareTag("Enemy"))
                        Debug.Log("Enemy" + hit.transform.name);
                    else {
                        Debug.Log("Not an Enemy");
                    }

                }

                nextFire = Time.time + fireRate;
                //Debug.Log("shootaniggersd");
            }
        }
    }
}


