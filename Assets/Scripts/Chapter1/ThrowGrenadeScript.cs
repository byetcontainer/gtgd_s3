﻿using UnityEngine;
using System.Collections;

namespace Chapter1 {
    public class ThrowGrenadeScript : MonoBehaviour
    {
        public GameObject grenadePrefab;
        public float force = 200;

        private s3.GameManager_TogglePause gameManagerMaster_togglePause;
        void Start() {
            SetReferences();
        }

        void SetReferences() {
            gameManagerMaster_togglePause = GameObject.Find("GameManager").GetComponent<s3.GameManager_TogglePause>();
        }

        void Update() {
            if (Input.GetButtonDown("Fire1") && !gameManagerMaster_togglePause.isPaused) {
                SpawnGrenade();
            }
            
        }

        void SpawnGrenade() {
            GameObject go = (GameObject) Instantiate(grenadePrefab, transform.TransformPoint(0,0,.75f), transform.rotation);
            go.GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);
            Destroy(go, 2f);
        }

    }

}

