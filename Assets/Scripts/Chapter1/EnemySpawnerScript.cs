﻿using UnityEngine;
using System.Collections;

namespace Chapter1 {
    public class EnemySpawnerScript : MonoBehaviour
    {
        public GameObject objectToSpawn;
        public int numberOfEnemies;
        private float spawnRadius = 5;
        private Vector3 spawnPosition;
        private GameManager_EventMasterScript eventMasterScript;

        void OnEnable() {
            SetReferences();
            eventMasterScript.myGeneralEvent += SpawnObject;
        }

        void OnDisable() {
            eventMasterScript.myGeneralEvent -= SpawnObject;
        }

        // Use this for initialization
        //void Start()
        //{
        //    SpawnObject();
        //}

        //// Update is called once per frame
        //void Update()
        //{

        //}

        void SetReferences()
        {
            eventMasterScript = GameObject.Find("GameManager").GetComponent<GameManager_EventMasterScript>();
        }

        void SpawnObject() {
            for (int i = 0; i < numberOfEnemies; i++) {
                spawnPosition = transform.position + Random.insideUnitSphere * spawnRadius;
                Instantiate(objectToSpawn, spawnPosition, Quaternion.identity);
            }
        }
    }

}
