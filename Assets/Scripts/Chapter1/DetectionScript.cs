﻿using UnityEngine;
using System.Collections;

namespace Chapter1 {
    public class DetectionScript : MonoBehaviour
    {
        private RaycastHit hit;
        private LayerMask detectionLayer;
        private float checkRate = 0.5f;
        private float nextcheck;
        private float range = 5;

        void Update() {
            SetReferences();
            DetectItems();
        }

        void SetReferences() {
            detectionLayer = 1 << 9;
        }

        void DetectItems()
        {
            if (Time.time > nextcheck) {
                nextcheck = Time.time + checkRate;

                if (Physics.Raycast(transform.position, transform.forward, out hit, range, detectionLayer)) {
                    Debug.Log(hit.transform.name + "is an item");
                }
            }
        }
    }
}


