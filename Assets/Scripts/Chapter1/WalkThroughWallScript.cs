﻿using UnityEngine;
using System.Collections;

namespace Chapter1 {
    public class WalkThroughWallScript : MonoBehaviour
    {

        private Color myColor = new Color(0.5f, 1, 0.5f, 0.25f);
        private GameManager_EventMasterScript eventMasterScript;

        void Start() {

        }

        void OnEnable()
        {
            SetInitialReferences();
            eventMasterScript.myGeneralEvent += SetLayerToNotSolid;
        }

        void OnDisable()
        {
            eventMasterScript.myGeneralEvent -= SetLayerToNotSolid;
        }


        void SetLayerToNotSolid() {
            gameObject.layer = LayerMask.NameToLayer("NotSolid");
            GetComponent<Renderer>().material.SetColor("_Color", myColor);
        }

        void SetInitialReferences() {
            eventMasterScript = GameObject.Find("GameManager").GetComponent<GameManager_EventMasterScript>();
        }
    }

}

