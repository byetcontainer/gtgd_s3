﻿using UnityEngine;
using System.Collections;

namespace Chapter1 {
    public class GrenadeExplosionScript : MonoBehaviour
    {
        private Collider[] hitColliders;
        public float blastRadius = 10;
        public float explosionPower = 200;
        public LayerMask explosionLayers;
        public float destroyTime = 7;

        void OnCollisionEnter(Collision col) {
            //Debug.Log(col.contacts[0].point.ToString());
            ExplosionWork(col.contacts[0].point);
            Destroy(gameObject);
        }

        void ExplosionWork(Vector3 explosionPoint) {
            hitColliders = Physics.OverlapSphere(explosionPoint, blastRadius, explosionLayers);

            foreach (Collider hitCol in hitColliders) {
                if (hitCol.GetComponent<NavMeshAgent>() != null) {
                   hitCol.GetComponent<NavMeshAgent>().enabled = false;
                }

                //Debug.Log(hitCol.gameObject.name);
                if (hitCol.GetComponent<Rigidbody>() != null) {
                    hitCol.GetComponent<Rigidbody>().isKinematic = false;
                    hitCol.GetComponent<Rigidbody>().AddExplosionForce(explosionPower, explosionPoint, blastRadius, 1, ForceMode.Impulse);
                }

                if (hitCol.CompareTag("Enemy")) {
                    Destroy(hitCol.gameObject, destroyTime);
                }
            }
        }

    }

}
