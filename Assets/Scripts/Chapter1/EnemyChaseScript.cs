﻿using UnityEngine;
using System.Collections;


namespace Chapter1 {
    public class EnemyChaseScript : MonoBehaviour
    {
        public LayerMask detectionLayer;
        private NavMeshAgent navmeshagent;
        private Collider[] hitColliders;
        private float checkRate;
        private float nextCheck;
        private float detectionRadius = 10;

        void Start() {
            SetReferences();
        }

        void Update() {
            CheckIfPlayerInRange();
        }

        void SetReferences() {
            navmeshagent = GetComponent<NavMeshAgent>();
            checkRate = Random.Range(0.8f, 1.2f);
        }

        void CheckIfPlayerInRange() {
            if (Time.time > nextCheck && navmeshagent == true) {
                nextCheck = Time.time + checkRate;

                hitColliders = Physics.OverlapSphere(transform.position, detectionRadius, detectionLayer);

                if (hitColliders.Length > 0 && GetComponent<NavMeshAgent>().enabled) {
                    navmeshagent.SetDestination(hitColliders[0].transform.position);
                }
            }

        }


    }

}
