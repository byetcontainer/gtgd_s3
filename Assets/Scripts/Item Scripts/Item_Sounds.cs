﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_Sounds : MonoBehaviour
    {
        private Item_MasterScript itemMaster;
        public float defaultVolume;
        public AudioClip throwSound;


        // Use this for initialization
        void OnEnable(){
            SetReferences();
            itemMaster.EventObjectThrow += PlayThrowSound;
        }

        void OnDisable() {
            itemMaster.EventObjectThrow -= PlayThrowSound;
        }

        void SetReferences() {
            itemMaster = GetComponent<Item_MasterScript>();
        }

        void PlayThrowSound() {
            if (throwSound != null) {
                AudioSource.PlayClipAtPoint(throwSound, transform.position, defaultVolume);
            }
        }
    }

}
