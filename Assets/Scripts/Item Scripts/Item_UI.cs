﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_UI : MonoBehaviour
    {
        private Item_MasterScript itemMaster;
        public GameObject myUI;

        void OnEnable() {
            SetReferences();
            itemMaster.EventObjectPickup += EnableMyUI;
            itemMaster.EventObjectThrow += DisableMyUI;
        }

        void OnDisable() {
            itemMaster.EventObjectPickup -= EnableMyUI;
            itemMaster.EventObjectThrow -= DisableMyUI;
        }

        void SetReferences() {
            itemMaster = GetComponent<Item_MasterScript>();
        }

        void EnableMyUI() {
            if (myUI != null) {
                myUI.SetActive(true);
            }
        }

        void DisableMyUI() {
            if (myUI != null)
            {
                myUI.SetActive(false);
            }
        }
        
    }

}
