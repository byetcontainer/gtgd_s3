﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_Pickup : MonoBehaviour
    {
        private Item_MasterScript itemMaster;

        void OnEnable() {
            SetReferences();
            itemMaster.EventPickupAction += CarryOutPickupActions;
        }

        void OnDisable() {
            itemMaster.EventPickupAction -= CarryOutPickupActions;
        }

        void SetReferences() {
            itemMaster = GetComponent<Item_MasterScript>();
        }

        void CarryOutPickupActions(Transform tParent) {
            transform.SetParent(tParent);
            itemMaster.CallEventObjectPickup();
            transform.gameObject.SetActive(false);
        }
    }

}
