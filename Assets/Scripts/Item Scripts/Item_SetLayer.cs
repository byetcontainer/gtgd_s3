﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_SetLayer : MonoBehaviour
    {
        private Item_MasterScript itemMaster;
        public string itemThrowLayer;
        public string itemPickupLayer;


        void OnEnable() {
            SetReferences();
            itemMaster.EventObjectPickup += SetItemToPickupLayer;
            itemMaster.EventObjectThrow += SetItemToThrowLayer;
        }

        void OnDisable() {
            itemMaster.EventObjectPickup -= SetItemToPickupLayer;
            itemMaster.EventObjectThrow -= SetItemToThrowLayer;
        }

        void Start() {
            SetLayerOnEnable();
        }

        void SetReferences() {
            itemMaster = GetComponent<Item_MasterScript>();
        }

        void SetItemToThrowLayer() {
            SetLayer(transform, itemThrowLayer);
        }

        void SetItemToPickupLayer() {
            SetLayer(transform, itemPickupLayer);
        }

        void SetLayerOnEnable() {
            if (itemPickupLayer == "") {
                itemPickupLayer = "Item";
            }

            if (itemThrowLayer == "") {
                itemThrowLayer = "Item";
            }

            if (transform.root.CompareTag(GameManager_References._playerTag)) {
                SetItemToPickupLayer();
            }
            else
            {
                SetItemToThrowLayer();
            }
        }

        void SetLayer(Transform tform, string itemLayerName) {
            tform.gameObject.layer = LayerMask.NameToLayer(itemLayerName);

            foreach (Transform child in tform) {
                SetLayer(child, itemLayerName);
            }
        }



    
    }

}
