﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_Rigidbodies : MonoBehaviour
    {
        private Item_MasterScript itemMaster;
        public Rigidbody[] rigidBodies;

        void OnEnable()
        {
            SetReferences();
            itemMaster.EventObjectThrow  += SetIsKinematicToFalse;
            itemMaster.EventObjectPickup += SetIsKinematicToTrue;
        }

        void OnDisable() {
            itemMaster.EventObjectThrow  -= SetIsKinematicToFalse;
            itemMaster.EventObjectPickup -= SetIsKinematicToTrue;
        }

        void Start() {
            CheckIfStartsInInventory();
        }

        void SetReferences() {
            itemMaster = GetComponent<Item_MasterScript>();
        }

        void CheckIfStartsInInventory() {
            if (transform.root.CompareTag(GameManager_References._playerTag)) {
                SetIsKinematicToTrue();
            }
        }

        void SetIsKinematicToTrue() {
            if (rigidBodies.Length > 0) {
                foreach (Rigidbody rb in rigidBodies) {
                    rb.isKinematic = true;
                }
            }
        }

        void SetIsKinematicToFalse() {
            if (rigidBodies.Length > 0)
            {
                foreach (Rigidbody rb in rigidBodies)
                {
                    rb.isKinematic = false;
                }
            }
        }
    }



}
