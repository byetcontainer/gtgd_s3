﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_Animator : MonoBehaviour
    {
        private Item_MasterScript itemMaster;
        public Animator myAnimator;

        void OnEnable() {
            SetReferences();
            itemMaster.EventObjectThrow  += DisableMyAnimator;
            itemMaster.EventObjectPickup += EnableMyAnimator;
        }

        void OnDisable() {
            itemMaster.EventObjectThrow  -= DisableMyAnimator;
            itemMaster.EventObjectPickup -= EnableMyAnimator;
        }

        void SetReferences() {
            itemMaster = GetComponent<Item_MasterScript>();    
        }


        void EnableMyAnimator() {
            if (myAnimator != null) {
                myAnimator.enabled = true;
            }
        }

        void DisableMyAnimator() {
            if (myAnimator != null)
            {
                myAnimator.enabled = false;
            }
        }

    }

}
