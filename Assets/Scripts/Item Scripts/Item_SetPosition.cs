﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_SetPosition : MonoBehaviour
    {
        private Item_MasterScript itemMaster;
        public Vector3 itemLocalPosition;

        void OnEnable() {
            SetReferences();
           
            itemMaster.EventObjectPickup += SetPositionOnPlayer;
        }

        void OnDisable() {
            itemMaster.EventObjectPickup -= SetPositionOnPlayer;
        }

        void Start() {
            SetPositionOnPlayer();
        }

        void SetReferences() {
            itemMaster = GetComponent<Item_MasterScript>();
        }

        void SetPositionOnPlayer() {
            if (transform.root.CompareTag(GameManager_References._playerTag)) {
                transform.localPosition = itemLocalPosition;
            }
        }
    }

}
