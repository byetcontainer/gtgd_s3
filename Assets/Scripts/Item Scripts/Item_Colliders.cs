﻿using UnityEngine;
using System.Collections;

namespace s3 {
    public class Item_Colliders : MonoBehaviour 
    {
        private Item_MasterScript itemMaster;
        public Collider[] colliders;
        public PhysicMaterial myPhysicMaterial;

        void OnEnable() {
            SetReferences();
            itemMaster.EventObjectThrow  += EnableColliders;
            itemMaster.EventObjectPickup += DisableColliders;
        }

        void OnDisable() {
            itemMaster.EventObjectThrow  -= EnableColliders;
            itemMaster.EventObjectPickup -= DisableColliders;
        }

        void Start() {
            CheckIfStartsInInventory();
        }



        void SetReferences() {
            itemMaster = GetComponent<Item_MasterScript>();
        }

        void CheckIfStartsInInventory()
        {
            if (transform.root.CompareTag(GameManager_References._playerTag)) {
                DisableColliders();
            }
        }

        void EnableColliders() {
            if (colliders.Length > 0) {
                foreach (Collider col in colliders) {
                    col.enabled = true;

                    if (myPhysicMaterial != null) {
                        col.material = myPhysicMaterial;
                    }
                }
            }
        }

        void DisableColliders() {
            if (colliders.Length > 0)
            {
                foreach (Collider col in colliders)
                {
                    col.enabled = false;
                }
            }
        }

    }

}
